
<header role="banner" style="border: none !important;">
    <h1 style="border: none !important;">Mobile Web Layout Testing</h1>
    <h6 style="border: none !important;">(testing-mobile-web-layouts)</h6>
</header>

> #### Testing the best way to create website layouts for mobile devices.

[![Netlify Status](https://api.netlify.com/api/v1/badges/1ed79004-6c31-4eba-b41c-25e4a99022c6/deploy-status)](https://app.netlify.com/sites/mobile-web-layout-testing/deploys)

### [Visit The Live Site, Hosted On Netlify](https://mobile-web-layout-testing.netlify.app/)

## Current Focuses
- Webapp Shell / Best page structure
- Mobile Navigation Menu
- Overall responsive navigation menu
- Light/Dark Color Schemes
- Support best touch interaction possible using HTML/SCSS/JS
- Optimize Typescale for easy reading, even (or especially) on mobile
